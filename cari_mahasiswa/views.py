from django.shortcuts import render
from login.models import User

# Create your views here.
response = {}
def index(request):
	user_list = User.objects.all().values()
	html = 'table.html'
	response = {"user_list" : user_list}
	return render(request, html, response)