"""mblepp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
import profile_app.urls as profile
import riwayat.urls as riwayat
import login.urls as login
import status.urls as status

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^profile/', include(profile, namespace='profile')),
    url(r'^riwayat/', include(riwayat, namespace='riwayat')),
    url(r'^login/', include(login, namespace='login')),
    url(r'^status/', include(status, namespace='status')),
    url(r'^$', RedirectView.as_view(url="/login/", permanent="true"), name='index'),
]
