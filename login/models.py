from django.db import models

# Create your models here.
class User (models.Model):
	nama = models.CharField('Nama', max_length=200)
	kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True)
	role = models.CharField('Role', max_length=100)
