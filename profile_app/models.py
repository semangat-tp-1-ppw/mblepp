from django.db import models

class Profile(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True)
    name = models.CharField(max_length=30, default='Kosong')
    link = models.URLField('Profile LinkedIn', default='Kosong')
    email = models.EmailField('Email', default='Kosong')


class Expertise(models.Model):
    user = models.ForeignKey(Profile)
    expert = models.CharField('Keahlian', max_length=27)


class Profile_linkedin(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True)
    firstName = models.CharField('Nama Depan', max_length=225)
    lastName = models.CharField('Nama Belakang', max_length=225)
    imageUrl = models.URLField('Foto')
    email = models.EmailField('Email', default='Kosong')
    profileUrl = models.URLField('Profile LinkedIn', default='Kosong')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Account(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True, )
    nama = models.CharField(max_length=200, default="NULL")
    created_at = models.DateTimeField(auto_now_add=True)


class Status(models.Model):
    account = models.ForeignKey(Account)
    name = models.CharField(max_length=27)
    message = models.TextField(max_length=300)
    created_date = models.DateTimeField(auto_now_add=True)

