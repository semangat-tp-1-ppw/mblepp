from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Profile, Status, Expertise, Account, Profile_linkedin
from login.views import response as resp
from login.csui_helper import response as resps
from django.utils import timezone

response = {'logged_in': False, 'login_cookie': False, 'profile_in_cookie': False}

def index(request):
	if resp['logged_in']:
		print(resps)
		response['author'] = 'Mblepp'
		response['name'] = Profile.name
		response['email'] = Profile.email
		response['expertise'] = Expertise.expert
		response['npm'] = Profile.kode_identitas

		kode_identitas = request.session['kode_identitas']
		if (kode_identitas not in Account.objects.filter(kode_identitas=kode_identitas)):
		    create_new_user(request)

		response['latestStatus'] =''
		response['latestStatusDate'] = ''
		response['jumlahAllStatus'] = 0
		response['name'] = Account.objects.get(kode_identitas=kode_identitas).nama
		allStatus = Status.objects.filter(account=Account.objects.get(kode_identitas=kode_identitas))
		response['allStatus'] = allStatus
		response['jumlahAllStatus'] = allStatus.count()
		if len(allStatus) > 0:
			response['latestStatus'] = allStatus.order_by('-created_date')[0].message
			response['latestStatusDate'] = allStatus.order_by('-created_date')[0].created_date

		else:
			response['latestStatus'] = ''
			response['latestStatusDate'] = ''

		return render(request, 'profile_app/profile.html', response)
	else:
	    return HttpResponseRedirect('/login/')


# Method tambahan untuk get informasi riawayat matkul dan nilainya
#def set_data_for_riwayat(res, request):
#    response['riwayat'] = get_riwayat().json()

def edit_profile(request):
    if 'user_login' in request.session:
        response['name'] = Profile.name
        html = 'edit_profile.html'
        return render(request, html, response)

def save_to_database(request):
    if(request.method == 'POST'):
        firstName = request.POST['firstName']
        lastName = request.POST['lastName']
        imageUrl = request.POST['imageUrl']
        email = request.POST['email']
        profileUrl = request.POST['profileUrl']
        kode_identitas = request.session['kode_identitas']
        exist = Profile_linkedin.objects.filter(kode_identitas=kode_identitas).exists()
        if not exist:
            user = Profile_linkedin(firstName=firstName, lastName=lastName, imageUrl=imageUrl, email=email, profileUrl=profileUrl, kode_identitas=kode_identitas)
            user.save()
        return JsonResponse({'save':'ok'})

def create_new_user(request):
    nama = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    pengguna = Account()
    pengguna.kode_identitas = kode_identitas
    pengguna.nama = nama
    pengguna.created_at = timezone.now()
    pengguna.save()

    return pengguna

def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data
