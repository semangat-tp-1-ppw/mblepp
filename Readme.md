# Anggota Kelompok:
	1. Fadhlan Hazmi
	2. Indra Septama
	3. Jovanta Anugerah Pelawi
	4. Naomi Riana Lumban Gaol

# Status and Coverage
	
[![pipeline status](https://gitlab.com/semangat-tp-1-ppw/mblepp/badges/master/pipeline.svg)](https://gitlab.com/semangat-tp-1-ppw/TP-1-PPW/commits/master)
[![coverage report](https://gitlab.com/semangat-tp-1-ppw/mblepp/badges/master/coverage.svg)](https://gitlab.com/semangat-tp-1-ppw/TP-1-PPW/commits/master)

# Link HerokuApp:
https://mbleppp.herokuapp.com

