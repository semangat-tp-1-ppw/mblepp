from django.db import models
from login.models import User

class StatusKu(models.Model):
	pengguna = models.ForeignKey(User, on_delete=models.CASCADE)
	description = models.TextField()
	created_date = models.DateTimeField(auto_now_add=True)
