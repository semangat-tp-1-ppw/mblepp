from django.shortcuts import render
from .models import StatusKu
from .forms import Form_status
from login.models import User
from django.http import HttpResponseRedirect
# Create your views here.
response = {}
def index(request, username):
	if get_data_user(request, 'user_login'):
		name = request.session.get('user_login', None)
		set_data_for_session(request)
		kode_identitas = get_data_user(request, 'kode_identitas')
		pengguna = User.objects.get(kode_identitas = kode_identitas)


		if (name == username):
			response['show_form'] = True
		else:
			response['show_form'] = False
		print('pengguna')
		response['test'] = 'hai halo'
		response['login'] = True
		statusnya = StatusKu.objects.filter(pengguna = pengguna).order_by('-id')
		response['status_form'] = Form_status
		response['status'] = statusnya
		html = 'status/status.html'
		return render(request, html, response)
	else:
		response['login'] = False
		html = 'login/login.html'
		return render(request, html, response)


def add_status(request):
	form = Form_status(request.POST or None)
	#set_data_for_session(request)
	

	#kode_identitas = get_data_user(request, 'kode_identitas')
	#pengguna = User.objects.get(kode_identitas = kode_identitas)
	if(request.method == 'POST' and form.is_valid()):
		username = request.session['user_login']
		pengguna = User.objects.get(nama = username)
		print('dia adalah=>',pengguna)
		response['description'] = request.POST['description']
		status = StatusKu(pengguna = pengguna, description=response['description'])
		#status.pengguna = pengguna
		status.save()
		return HttpResponseRedirect('/status/'+username)
	else:
		return HttpResponseRedirect('/status/'+username)

def delete_status(request, id_status):
	print('delete something')
	status = StatusKu.objects.get(pk = id_status)
	status.delete()
	return HttpResponseRedirect('/status/'+username)

def get_data_user(request, tipe):
	data = None
	if tipe == "user_login" and 'user_login' in request.session:
		data = request.session['user_login']
	elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
		data = request.session['kode_identitas']
	elif tipe == "role" and 'role' in request.session:
		data = request.session['role']


	return data

def set_data_for_session(request):
	response['author'] = get_data_user(request, 'user_login')
	response['kode_identitas'] = request.session['kode_identitas']
	response['role'] = request.session['role']
